

# Mulesoft Icons for Architecture drawings
# Now migrated to https://github.com/Deloitte-UK-Innersource/mulesoft-icons

## What this is

A base library of Mulesoft images for use in Presenations and architecture drawings

Prioritisation of high quality SVG with alternative options into PNG formats using generation tools.


## Usage Options

### Mule Colours 

<img src="./src/main/resources/images/svg/MULECOLOURS.svg" alt="Mule Colours"/>

```
#3A3B3C
#989A9B
#58595A
#087299
#00A3E0
#114459
```

### PREVIEW the Icons

You can view the list of SVG Icons at [svg_images.md](./svg_images.md) , the sources are in folder `./src/main/resources/images/svg`

*Where to get*
`src\main\resources\images\svg\ExtractedSVGs\pure`

[src\main\resources\images\svg\ExtractedSVGs\pure](./src/main/resources/images/svg/ExtractedSVGs/pure)

### Powerpoint sample file

*If you must...* there's a PPT file here with a set of Muley icons

![PowerpointIcon](./src/main/resources/powerpoint/powerpoint.png) [Mulesoft_Icons.pptx](./src/main/resources/powerpoint/MuleSoft_Icons.pptx)

The file above has a set of useful images that can be used directly in powerpoint presentations, but does not currently contain all SVG's:

<img src="./src/main/resources/powerpoint/Mulesoft_Icons.gif"/>

### The draw.io Specification


*Latest* 

`.src/main/resources/drawio/NewMuleLibrary.xml`

<img src="./src/main/resources/images/svg/examples/Mule_app_diagrams_net.png" width="600px" height="400px">


Each image is a base64 encoded `data` element with some attributes:

[draw.io Create your own custom library](https://drawio-app.com/draw-io-training-exercise-9-create-your-own-custom-library/)

```
<!-- Top Level XML -->
<mxlibrary>
   <!-- Array -->
   [
   <!-- Image Example -->
     {
      "data" : "data:image/png;base64,iVBORw....................",
      "w":167,
      "h":167,
      "title":"X",
      "aspect":"fixed"
     } 
   ]
</mxlibrary>

```


### PlantUML Mulesoft Icons

Influenced by C4 Oriented examples such as the Azure Icon library for PlantUML - it can be done with Mule Icons!

You can use the PlantUML WebServer to experiment - must include the Common PUML and then add further icons to create diagrams;

*Sample*
<img src="./src/main/resources/images/samplePUML.png"/>


Example Configuration URL
[PUML: Mule Functional Monitoring](http://www.plantuml.com/plantuml/uml/hLRzR-8y4lwTls8sFQdNEyX70ade0HAqwR8wQAj2jzgxFI43XaHhx5nYeDrNx__-ZXtQGCj-hBHHFnpFpFCyCxOdFeLBoaGY9QqDrup2BIDStWy7LTZGB4y41zjqJBjo2dTqyI-up-aS5YA3XETfaX6fF68fq_pSifQ9ZDTJSoPMLgfWrYjMo-MS9LEh-tMTqTepk3LbOcgjI2vfPisJN1Rs2x5Q2Mwc65T1mWl1H9RZuEI7JwNoRauN2QSml1z4a_TtqIS7Jhokr_CktffSE6c6pMwpzzRamKaZR3GRtKDJRt0V8Q0T1cwp-TRaeidkUf7zreI0ZbElDvegev5yOWbV_c9Au-YYVpsvkhaUJqOV_XE1Kz8Ckhrei6_oNqoZSNGNtOm_tKRxTWTbZ6CAaZvA82nPyXNbKgyeOQoObZ6HC1Sq1ouan6H3zpnpAc6Bwmm4yYy9Jqb6LfMN4I0vMJCvHfpkIymC_z2iO4OgdE5aHNzCMleUfJNVybvXmBNoFxr9AOSHuVdfKLxDcN38w4mIlcGqBy7AoUDmfUCs8-b3CaTPhcqZpv0y9kiLvCbNgi3Jv96ol0fY0GHMDC_9KizZ3lJ_Qyfd5EO9MHvMS4KUXuNhIC4u3RjoJ1hyKG5yHbXOAkHJghLYnF65h_ofjjzbKIJePfdOmWX1RnLehYjo-KP9uLRmwBC89xv-z8pYEpvRw7OLvsupCi6NeDfBn6KYdtGOdeE_qqckzPyTox3LUZPtEZiLFJBxiip4ciyBveSuaRImY6nEisAsnz2jg4vCvcABJ22pTU5uLqHdd6RWs7hgs2gZ-7UKpqlvgdu3yYJMybSxqg3xwUP-F1dVt4ukRnwkGOeKVs4gf1IuSHELS2BuQ_z1T3MUt7rutny3emkfGh9a6Sio0YMDlb0vYo77la9tyg06VSBdsqprT5u5RAh9JELKXMiXQKfIpFoVS9cHXSIGAQC7gxHZxuucetz7uul-tifCNrN_jdjtqHz6r-CzdxYaBch-xIDgQ6Mwt30S6PMYIy122FMlfS_hJ16rCphlF92CuqAUjonbw1JwTnitdwrpzQawicGHeolAVtRDCEBuZiMTIbX2i8PqmiYKCZMGT1PpDT0grI1FCNLQXN9VdvO7rcYtTy6VUKk7dEz2Ews_FSTsMbOnzPbRLairalDNxR_ptntp_rgMidSiou23iq9oAEHYrgyhyhlIqoC4a6MoQgIoxItrzZYh7AJ-dCshBrPdH_YIotTgyuzztjzRg4FfR_PHfU25nPhFXe8dKhSlQEMR9MnL_syRRiCt8ARgXEvUDWdTziHZsx311Wz_70CM2MDjWmjE3NXSCPwt3NLvmhlJThirjvufigNbshPj8RJHQQL4nZ1l6qF7CVqm0DSr6utwHtop7JzsJSyED_ZIyCAufj--FdFixYg6jG_Yeult31AQWHrgZ63ZU6POzmkCeCGeBZK7685f1-uEObDhc47GRvfUkD4ZRIp3YulFGRZhcQxhWrCtWxg_GJx_HVh6nvS6q-EQPpQRtnVIDCCGtC1qWbXLe1umtxGzj-ROPhtUh3ccXoywdveoUA0DOJz4vKnP7T3Mxyh5iWGOwPk1NnyWenU2RzRxBX8oJU5f2erg-mCNTJWgkNh3rnmkQFEnHDGATogL8r_5mrrsfkulMP8oMjmhn0BS-gDRXz77z_1XjlVXK51NQww_3OIL2Dy-tMS_sGFAItNrrHceWbS5OuKevPo1CoTN-Z5GVUMRklQWCAHqWKCi5P-BXJJE_W80)


This example is a concatenation of:

 * `./src/main/resources/plantuml/MuleCommon.puml`
 * `./src/main/resources/plantuml/mulesoft/functional-monitoring.puml`


<img src="https://plantuml.com/logo3.png" width="64" height="64" alt="PlantUML"/>


*Root / Common file*

This is the seed file, need this for any other icon

[./src/main/resources/plantuml/MuleCommon.puml](./src/main/resources/plantuml/MuleCommon.puml)

*Directory* (More coming soon!)

[./src/main/resources/plantuml/mulesoft](./src/main/resources/plantuml/mulesoft)




## Possible tools

 * [PNG to SVG converter online](https://www.pngtosvg.com/)
 * [Base64 PNG image viewer](https://jaredwinick.github.io/base64-image-viewer/)
 * [Maven SVG image generator](https://github.com/uphy/svg-maven-plugin)
 * [Draw.io custom library](https://drawio-app.com/draw-io-training-exercise-9-create-your-own-custom-library)

## Development Suggestions

 * Check current SVG images all render OK
 * Parse out images from APIManager2.svg (129 images)
 * Gather any further SVG images - mulesoft samples, online
 * Convert the PNG native images in draw.io library to SVG (or find source)
 * Clean any out of date images from the draw.io sample set
 * Use a Maven/Gradle script to bulk-convert to PNG at different size (16,32,48,64,128,256px) - High res
 * Update the powerpoint with newly sourced images
 * Add metadata to images based on architecture notes, Mule refs
 * Package a new draw.io library for added images, try use base64 encoded SVG as 1st pref
 * Package a Javascript font library or extension to framework like ironfonts, fontawesome
   ** [Customise fontawesome](https://github.com/FortAwesome/Font-Awesome/wiki/Customize-Font-Awesome)
 * Package a Visio image library
 * ...


## SVG Samples


### Anypoint Service Mesh

32
<img src="./src/main/resources/images/svg/ExtractedSVGs/pure/Anypoint-service-mesh.svg" alt="Anypoint Service Mesh" width="32" height="32"/>
64 
<img src="./src/main/resources/images/svg/ExtractedSVGs/pure/Anypoint-service-mesh.svg" alt="Anypoint Service Mesh" width="64" height="64"/>
128 
<img src="./src/main/resources/images/svg/ExtractedSVGs/pure/Anypoint-service-mesh.svg" alt="Anypoint Service Mesh" width="128" height="128"/>


### Data Gateway

32
<img src="./src/main/resources/images/svg/ExtractedSVGs/pure/data-gateway-color.svg" alt="Data Gateway" width="32" height="32"/>
64 
<img src="./src/main/resources/images/svg/ExtractedSVGs/pure/data-gateway-color.svg" alt="Data Gateway" width="64" height="64"/>
128 
<img src="./src/main/resources/images/svg/ExtractedSVGs/pure/data-gateway-color.svg" alt="Data Gateway" width="128" height="128"/>





## Extract SVGs from a SVG-Sprite
An [SVG sprite](https://css-tricks.com/svg-sprites-use-better-icon-fonts/) is an entire collection of SVGs (coloured, greyscale, various sizes etc.) into a single SVG file. This allows creaters to inject desired graphics onto a webpage by referencing just the ID from a single sprite file. 

Sometimes you may get a sprite from a source but all you really want is to extract the independent SVGs to either browse through them or use them as desired. A nifty script has been added at [./src/main/node/ExtractFromSprite](./src/main/node/ExtractFromSprite) to do this. Steps to follow:
1. Cleanse your SVG sprint file from [SVG Sanitizer Test (enshrined.co.uk)](https://svg.enshrined.co.uk/)
2. Put the cleansed SVG in sprite.svg and save it in the same directory as the script.js
3. execute ``` node script.js```
4. Output will be saved in the [output](./src/main/node/ExtractFromSprite/output) folder

Script:
```
var fs = require('fs');
var path = require('path');

var markup = fs.readFileSync('sprite.svg').toString();
var lines = markup.split(/\n/g);
var symbols = {};
var currentSymbol = null;

lines.forEach(function(line){
    var open = line.match(/symbol.*?id="(.*?)"/);
    var close = line.match(/<\/symbol>/);
    if(currentSymbol){
        symbols[currentSymbol].push(line);
    }
    if(open){
        currentSymbol = open[1];
        symbols[currentSymbol] = [line];
    }
    if(close){
        symbols[currentSymbol] = symbols[currentSymbol].join('\n').replace('<symbol', '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"').replace('</symbol', '</svg');
        fs.writeFileSync(path.join(__dirname, 'output/' + currentSymbol + '.svg'), symbols[currentSymbol]);
        currentSymbol = null;
    }
});
console.log( Object.keys(symbols) );
```

Source: https://gist.github.com/dennishall1/3e96d52e73db20b27cd0






## Segregate Pure vs Impure SVGs
At times SVGs received from unknown sources are not really scalable but rather are just an SVG package containing the icon embedded as a PNG BASE64 of its image. We need to identify such SVGs (impure) from true SVGs (pure). The approach to achieve this relatively simple, we to a string match to see if the 'base64' exists in the SVG when read as string. If it exists, we out this into an impure folder.
A script has been added at [./src/main/node/SegregatePureSVGs](./src/main/node/SegregatePureSVGs) to do this. Steps to follow:

1. Put the segregate.js into the folder where you want this segregation to happen
2. at the same location create 2 folers 'impure' and 'pure'. Notice all smallcase in the folder names
3. Run the script and let it segregate

Script:
```
var fs = require('fs');
var path = require('path');
fs.readdir(__dirname, function (err, files) {
    if (err) {
        return console.log('Unable to scan directory: ' + err);
    } 
    files.forEach(function (file) {
		fileName = file.toString().split('/').pop();
		if((fileName!='impure') && (fileName!='pure') && (fileName!='segregate.js')){
			var rawString = fs.readFileSync(file.toString().split('/').pop()).toString();
		var string = rawString.match(/base64/);
		if(string){
			fs.rename(file, path.join(__dirname, 'impure',file.toString().split('/').pop()), function (err) {
			if (err) throw err
				console.log('Successfully moved to impure')
			})
		} else{
			fs.rename(file, path.join(__dirname, 'pure',file.toString().split('/').pop()), function (err) {
			if (err) throw err
				console.log('Successfully moved to pure')
			})
        }
		}
        
    });
});
```
1. After running this script, the final set of SVGs reside in the [ExtractedSVGs](./src/main/resources/images/svg/ExtractedSVGs) folder. 
2. It has been observed that all SVGs inside the impure folder are extracted from the [Draw.io custom library](https://drawio-app.com/draw-io-training-exercise-9-create-your-own-custom-library)
3. All impure SVGs have a similar design language of an icon within a circle which does not sit will other open type icons and hence this folder an be discarded
4. We have 308 pure SVGs and all names have been polished to reflect what they contain

## Generate to PNG sizes 64x64, 128x128

The `pom.xml` maven targets run a bulk SVG to PNG generation currently to 64x64,128x128 px icons.

The files from the folder `src/main/resources/images/svg/ExtractedSVGs/pure` are the sources used in the conversion.

The generation process uses the plugins [svg-maven-plugin](https://github.com/uphy/svg-maven-plugin) and the [maven-antrun-plugin](https://maven.apache.org/plugins/maven-antrun-plugin/) using the Ant [copy](https://ant.apache.org/manual/Tasks/copy.html) task.

`mvn` phases are `generate-sources` and `generate-resources`

```
mvn package

// Files are generated as rasterized into
target/generated-images/png/64
target/generated-images/png/128
```


## Sample sites / repos for Libraries, Font approaches

The following are reference sites that have organised libraries of images we could use for inspiration

 * https://github.com/awslabs/aws-icons-for-plantuml/blob/master/AWSSymbols.md
 * https://github.com/awslabs/aws-icons-for-plantuml/tree/master
 * https://github.com/awslabs/aws-icons-for-plantuml/tree/master/scripts
 * https://crashedmind.github.io/PlantUMLHitchhikersGuide/aws/aws.html
 * https://crashedmind.github.io/PlantUMLHitchhikersGuide/PlantUMLSpriteLibraries/plantuml_sprites.html




## Images Hacks

### Where were the images from?

We got the Powerpoint from a Mulesoft contact

The draw.io library was available from [Github repository gui1207/draw.io](https://github.com/gui1207/draw.io)

Some SVG images were copied from the Portal directory and saved.



### Fixing SVG rendering, syntax

To fix some render issues, can use an SVG syntax checker like:

https://svg.enshrined.co.uk/


### Create a README index of the images

This was how the `svg_images.md` was created, can be useful to show the full set of icons

**Create a list of files in the director**
```
cd ./src/main/resources/images/svg/ExtractedSVGs/pure
ls -1 > ../../../../../../../svg_images.md
```

**Search / Replace**
REGEX
```
^[\w,-]*.svg
$0 ![$0](./src/main/resources/images/svg/ExtractedSVGs/pure/$0)
```

Output sample for each images
```
![3-dot-icon.svg](./src/main/resources/images/svg/ExtractedSVGs/pure/3-dot-icon.svg)
```

### Adding metadata to SVG's

It can be possible to add `<metadata>` , `<title>` and `<desc>` elements to SVG's which should be considered for additional context of the image meaning, source, history.

[SVG Schema - Mozilla.org](https://developer.mozilla.org/en-US/docs/Web/SVG/Element/metadata)

The images from the original Sources seem to have an `id` attribute and / or filename which could be re-used.

```
<svg width="400" viewBox="0 0 400 300"
  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    
    <!-- How to add metadata example -->
    <metadata>
    ...
    </metadata>
    <title>Anypoint Studio</title>
    <desc>Mulesoft Anypoint Studio Icon</desc>
    
    <path fill="#00A3E0" ...></path>

</svg>    
```

### SVG Sizing, Colours

[`<svg viewbox="  ">`](https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/viewBox) attribute gives a relatived sized bounding box for the SVG image

[`<svg style=" ">`](https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/style) is another way to influence style, colours, size for different elements.


#### SVG as base 64

Convert the SVG file to base64 and include after the comma `,` seems to work, this might give an improved result in draw.io or other libraries for Vector graphics

[SVG to Base64 converter](https://base64.guru/converter/encode/image/svg)

[Java 8 Base64 Endcode Decode](https://www.baeldung.com/java-base64-encode-and-decode)

```
data:image/svg+xml;base64,<base64 string.....>
```


### Changing inline PNG size in an SVG

Some of the images have just rendered a PNG inside an SVG,.. but it is possible to change the size. This gives an image size more useful to use as a default than a 32x30 default size.

See editing done in `svgexport-97.png` - changing the image width, height variables in the image tag and then aligning to the corresponding SVG style in same OOM created a larger image.

On the converted image apears to be 2^ multiples of `26.72` (or similar) are required.

```
<?xml version="1.0" encoding="UTF-8"?>
<svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" style="left: 1px; top: 1px; width: 256px; height: 256px; display: block; position: relative; overflow: hidden;" width="256" height="256">
  <g>
    <g></g>
    <g>
      <g style="visibility: visible;">
        <image x="2.56" y="1.6" width="213.76" height="213.76" xlink:href="data:image/png;base64,....................</image>
      </g>
    </g>
    <g></g>
    <g></g>
  </g>
</svg>
```

#### Multiples of 8 for internal Image width attribute


Possible pattern matches 
- replace the 32,20px, width = 32 height = 30 with 256
- replace the width="26.88" height="26.88" with width="215.04" height="215.04"

**Sample pattern match**
```
style="left: 1px; top: 1px; width: 32px; height: 30px; display: block; position: relative; overflow: hidden;" width="32"  height="30"

<image x="2.56" y="1.44" width="215.04" height="215.04"
```
------------------------------------------------------
------------------------------------------------------

# PlantUML Icon-Font Sprites

## Getting Started

The common.puml is required for the rest to work.

```puml
!include ../common.puml
```

or via url

```puml
!define ICONURL https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/v2.4.0
!includeurl ICONURL/common.puml
```

Import the sprites that you want

```puml
!include ../devicons/mysql.puml
!include ../font-awesome/database.puml
!include ../font-awesome-5/database.puml
```

or via url

```puml
!includeurl ICONURL/common.puml
!includeurl ICONURL/devicons/mysql.puml
!includeurl ICONURL/font-awesome/database.puml
!includeurl ICONURL/font-awesome-5/database.puml
```

To use the sprites add one of the macros

```puml
DEV_MYSQL(db)
```

The macros are prefixed with the set and the name of the icon

```puml
<prefix>_<name>(alias)
<prefix>_<name>(alias,label)
<prefix>_<name>(alias,label,shape)
<prefix>_<name>(alias,label,shape,color)
```

Using the icon from devicons for mysql

```puml
DEV_MYSQL(db1)
DEV_MYSQL(db2,label of db2)
DEV_MYSQL(db3,label of db3,database)
DEV_MYSQL(db4,label of db4,database,red) #DeepSkyBlue
```

![overload-example](examples/overload-example.png)

## Icon Sets

The following icon sets are included:

| Name                                                       | Index                                     |
| ---------------------------------------------------------- | ----------------------------------------- |
| [Font-Awesome 4](https://fontawesome.com/v4.7.0/)          | [List of macros](font-awesome/index.md)   |
| [Font-Awesome 5](http://fontawesome.io/)                   | [List of macros](font-awesome-5/index.md) |
| [Devicons](http://vorillaz.github.io/devicons)             | [List of macros](devicons/index.md)       |
| [Govicons](http://govicons.io/)                            | [List of macros](govicons/index.md)       |
| [Weather](https://erikflowers.github.io/weather-icons/)    | [List of macros](weather/index.md)        |
| [Material](http://google.github.io/material-design-icons/) | [List of macros](material/index.md)       |
| [Devicon 2](https://github.com/devicons/devicon.git)       | [List of macros](devicons2/index.md)      |

## Example

```puml
@startuml

skinparam defaultTextAlignment center

!define ICONURL https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/v2.4.0

!includeurl ICONURL/common.puml
!includeurl ICONURL/devicons/mysql.puml
!includeurl ICONURL/font-awesome/server.puml
!includeurl ICONURL/font-awesome-5/database.puml

title Styling example

FA_SERVER(web1,WEB1) #Green
FA_SERVER(web2,WEB1) #Yellow
FA_SERVER(web3,WEB1) #Blue
FA_SERVER(web4,WEB1) #YellowGreen

FA5_DATABASE(db1,LIVE,database,white) #RoyalBlue
DEV_MYSQL(db2,SPARE,database) #Red

db1 <--> db2

web1 <--> db1
web2 <--> db1
web3 <--> db1
web4 <--> db1

@enduml
```

![styling-example](examples/styling-example.png)

More examples can be found [here](examples/)

## Build

### Windows

```bash
npm install --global --production windows-build-tools
yarn install
yarn build
```

### Linux

```bash
apt install librsvg2-bin openjdk-11-jre graphviz
yarn install
yarn build
```

## Note

- All brand icons are trademarks of their respective owners.
- Thanks to milo-minderbinder for [AWS-PlantUML](https://github.com/milo-minderbinder/AWS-PlantUML)
- Thanks to [yuri-becker](https://github.com/yuri-becker) for the integration of [Devicon 2](https://konpa.github.io/devicon/)

## Contributing

Contribution is welcome. In order to update an existing font or to add a new font please fork the repository and use a feature branch.

## Changelog

### v2.4.0

- Updated devicons2 & pinned to v2.12.0
- Integrated project [font-icon-generator](https://github.com/tupadr3/font-icon-generator) into this project to make it easier to contribute

### v2.3.0

- Fixed wrong link in readme to devicons2
- Changed Repo for devicons2 to <https://github.com/devicons/devicon.git>
- Removed old dir "dev2"
- Updated FA5 to latest v5.15.3
- Updated devicons2 to latest version
- Pinned gov to 5.15.3

### v2.2.0

- Updated all except material to latest version
- Updated material to 3.0.2

### v2.1.0

- Added Devicon 2

### v2.0.0

- Added fa5, weather, gov and material
- Updated dev and fa to latest version
- Fixed aspect ratios

### v1.0.0

- Intital release

Enjoy!
