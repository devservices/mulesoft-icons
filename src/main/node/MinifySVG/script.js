// Use the library svgo - SVG Optimiser https://www.npmjs.com/package/svgo
const { loadConfig } = require('svgo');
//const config = await loadConfig('./minifyConfig.js', cwd);

//const { optimize } = require('svgo');

// you can also specify a relative or absolute path and customize the current working directory
//const config = await loadConfig(configFile, cwd);


var fs = require('fs');
var path = require('path');

// Loop over directory
var filenames = fs.readdirSync('../resources/images/svg/ExtractedSVGs/pure')

console.log("\nSVG filenames:");
filenames.forEach(file => {
  console.log(file);
});


//var markup = fs.readFileSync('sprite.svg').toString();


//const result = optimize(svgString, {
  // optional but recommended field
  //path: 'path-to.svg',
  // all config fields are also available here
  //multipass: true,
//});
//const optimizedSvgString = result.data;