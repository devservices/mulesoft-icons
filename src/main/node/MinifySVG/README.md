# How to Minify

Use the npm package svgo

https://www.npmjs.com/package/svgo


Create a copy of the 'pure' SVG's into a minified directory

```
svgo -f $(pwd)/src/main/resources/images/svg/ExtractedSVGs/pure -o $(pwd)/src/main/resources/images/svg/ExtractedSVGs/pureMinified
```

## Where to go next

Create a PlantuML inline SVG Sprte set

https://plantuml.com/sprite


For each '*.svg' in .`/src/main/resources/images/svg/ExtractedSVGs/pureMinified`

--> `sprite $filename $svgdata \n`

```
@startuml
sprite foo1 <svg width="8" height="8" viewBox="0 0 8 8"><path d="M1 0l-1 1 1.5 1.5-1.5 1.5h4v-4l-1.5 1.5-1.5-1.5zm3 4v4l1.5-1.5 1.5 1.5 1-1-1.5-1.5 1.5-1.5h-4z" /></svg>

Alice->Bob : <$foo1*3>
@enduml
```

Then Define an SVG Icon shortcut name save as a .puml file

There is an itemised CSV list of the images at `./src/main/resources/images/svg/ExtractedSVGs/image_metadata.csv`

```
!define DEV_ANDROID(_alias) ENTITY(rectangle,black,android,_alias,DEV ANDROID)
!define DEV_ANDROID(_alias, _label) ENTITY(rectangle,black,android,_label, _alias,DEV ANDROID)
!define DEV_ANDROID(_alias, _label, _shape) ENTITY(_shape,black,android,_label, _alias,DEV ANDROID)
!define DEV_ANDROID(_alias, _label, _shape, _color) ENTITY(_shape,_color,android,_label, _alias,DEV ANDROID)
skinparam folderBackgroundColor<<DEV ANDROID>> White
```

Samples:

A `Common.puml`
https://raw.githubusercontent.com/Crashedmind/PlantUML-opensecurityarchitecture2-icons/master/Common.puml

An `all.puml` using Includes:
https://raw.githubusercontent.com/Crashedmind/PlantUML-opensecurityarchitecture2-icons/master/all.puml

## Usage example

```
!define mulePuml https://raw.githubusercontent.com/Crashedmind/PlantUML-opensecurityarchitecture2-icons/master/puml
!include mulePuml/Common.puml
!include mulePuml/all.puml

listsprites

MULE_ANYPOINT(ap, "Anypoint)

```