var fs = require('fs');
var path = require('path');
fs.readdir(__dirname, function (err, files) {
    if (err) {
        return console.log('Unable to scan directory: ' + err);
    } 
    files.forEach(function (file) {
		fileName = file.toString().split('/').pop();
		if((fileName!='impure') && (fileName!='pure') && (fileName!='segregate.js')){
			var rawString = fs.readFileSync(file.toString().split('/').pop()).toString();
		var string = rawString.match(/base64/);
		if(string){
			fs.rename(file, path.join(__dirname, 'impure',file.toString().split('/').pop()), function (err) {
			if (err) throw err
				console.log('Successfully moved to impure')
			})
		} else{
			fs.rename(file, path.join(__dirname, 'pure',file.toString().split('/').pop()), function (err) {
			if (err) throw err
				console.log('Successfully moved to pure')
			})
        }
		}
        
    });
});