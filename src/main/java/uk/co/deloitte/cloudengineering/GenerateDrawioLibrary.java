package uk.co.deloitte.cloudengineering;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import uk.co.deloitte.cloudengineering.model.DrawIOImageElement;
import uk.co.deloitte.cloudengineering.model.DrawIOLibrary;

public class GenerateDrawioLibrary {

	public static void main(String [] args)
	{
		
		File f = new File("src/main/resources/images/svg/ExtractedSVGs/pure");
		try {
			readDirectory(f);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			throw new RuntimeException(e);
		}
		//String outString = createBase64SVGString(f);
		
		//System.out.println(outString) ;
	}
	
	
	private static void readDirectory(File directory) throws ParserConfigurationException, SAXException, IOException
	{
		
		DrawIOLibrary library = new DrawIOLibrary();
		
		// Read the directory
		
		// For each SVG File
		for( File f: directory.listFiles( ))
		{
			DrawIOImageElement image = new DrawIOImageElement();
		    image.setTitle(f.getName());
		    
		    
		    SAXParserFactory factory = SAXParserFactory.newInstance();
		    SAXParser saxParser = factory.newSAXParser();

		    SVGImageParser parser = new SVGImageParser();
		    parser.setDrawIOImage(image);
		    
		    saxParser.parse(f, parser);
		    
		    // hardcode to 'fixed'
		    image.setAspect("fixed");
		    
			   // Filename = title
			   // width
			   // height
			   // title
			   // base64 convert
		    
		    // Base64 encode the SVG image
		    image.setData(  createBase64SVGString(f));
			f.getName();
			// add the images
			library.addImage(image);
		}
		
		    
		generateDrawioLibraryFile(library);    

		   
	}
	
	private static void generateDrawioLibraryFile(DrawIOLibrary library) {
		// TODO Auto-generated method stub
		
		StringBuilder stringBuilder = new StringBuilder();
		
		
		stringBuilder.append( "<mxlibrary>["  );
		
		for (DrawIOImageElement element : library.getImages())
		{
			stringBuilder.append("" + imageToJson(element));
			
			// If it's last one don't do this
			stringBuilder.append(",\n");
		}
		
		// Delete last comma
		stringBuilder.deleteCharAt(  stringBuilder.length() -2);
		
		stringBuilder.append("]\n</mxlibrary>");
		

	    try {
	        //Path tempFile = Files.createTempFile(image.getTitle(), ".svg");

	        String tempPath = System.getProperty("java.io.tmpdir");
	        Path tempFilePath = Paths.get(tempPath + "NewMuleLibrary.xml");
	        System.out.println(tempFilePath.toString());
	        Path tempFile = Files.createFile(tempFilePath);

	  
	        Files.write(tempFile, stringBuilder.toString().getBytes(StandardCharsets.UTF_8));
	    } catch (IOException e) {
	        throw new RuntimeException(e);
	    }
		
	}
	


	private static String imageToJson(DrawIOImageElement element) {
		// TODO Auto-generated method stub
		
		//{"data":"data:image/png;base64,
		//w":256,"h":256,"title":"Api","aspect":"fixed"},

		String xx = "{\"data\":\"data:image/svg+xml;base64," + element.getData() + "\", " +
		 "\"w\":\"" + element.getW() + "\"," +
		 "\"h\":\"" + element.getH() + "\"," +
		 "\"title\":\"" + element.getTitle() + "\"," +
		 "\"aspect\":\"fixed\"" +
		"}";
		
		return xx;
	}


	
	
	private static String createBase64SVGString(File svgFile)
	{
		
		Path filePath = Path.of(svgFile.getAbsolutePath());
		StringBuilder contentBuilder = new StringBuilder();
		
		String content;
		try {
			content = Files.readString(filePath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException(e);
		}
		
        byte[] imageData = Base64.getEncoder().encode(content.getBytes());
        return new String(imageData);
	}
	
	
}
