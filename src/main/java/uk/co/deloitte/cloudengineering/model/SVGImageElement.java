package uk.co.deloitte.cloudengineering.model;


public class SVGImageElement
{

  private Integer width;
  private Integer height;
  private String viewBox;
  private String title;
  private String desc;
  private Object metadata;
  private String internalXML = new String();

  public Integer getWidth() {
    return width;
  }

  public void setWidth(Integer width) {
    this.width = width;
  }

  public Integer getHeight() {
    return height;
  }

  public void setHeight(Integer height) {
    this.height = height;
  }

  public String getViewBox() {
    return viewBox;
  }

  public void setViewBox(String viewBox) {
    this.viewBox = viewBox;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDesc() {
    return desc;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }

  public Object getMetadata() {
    return metadata;
  }

  public void setMetadata(Object metadata) {
    this.metadata = metadata;
  }

  public String getInternalXML() {
    return internalXML;
  }

  public void setInternalXML(String internalXML) {
    this.internalXML = internalXML;
  }
}

/**
<svg width="400" viewBox="0 0 400 300"
  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    
    <!-- How to add metadata example -->
    <metadata>
    ...
    </metadata>
    <title>Anypoint Studio</title>
    <desc>Mulesoft Anypoint Studio Icon</desc>
*/
