package uk.co.deloitte.cloudengineering.model;

import java.util.List;

public class DrawIOLibrary {
	private List<DrawIOImageElement> elements = new java.util.ArrayList<DrawIOImageElement>();

	public List<DrawIOImageElement> getImages() {
		return elements;
	}

	public void addImage(DrawIOImageElement element) {

		elements.add(element);
	}
}

/**
 * <!-- Top Level XML --> <mxlibrary> <!-- Array --> [ <!-- Image Example --> {
 * "data" : "data:image/png;base64,iVBORw....................", "w":167,
 * "h":167, "title":"X", "aspect":"fixed" } ] </mxlibrary>
 */