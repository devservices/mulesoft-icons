package uk.co.deloitte.cloudengineering.model;

public class DrawIOImageElement {

	private String data;
	// " : "data:image/png;base64,iVBORw....................",
	private String w;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getW() {
		return w;
	}

	public void setW(String w) {
		this.w = w;
	}

	public String getH() {
		return h;
	}

	public void setH(String h) {
		this.h = h;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAspect() {
		return aspect;
	}

	public void setAspect(String aspect) {
		this.aspect = aspect;
	}

	// "w":167,
	private String h;
	// "h":167,

	private String title;
//    "title":"X",

	private String aspect;
//    "aspect":"fixed"

}
