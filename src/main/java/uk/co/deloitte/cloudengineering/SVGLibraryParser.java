package uk.co.deloitte.cloudengineering;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import uk.co.deloitte.cloudengineering.model.SVGImageElement;
import uk.co.deloitte.cloudengineering.model.SVGLibrary;

import java.util.List;

public class SVGLibraryParser extends DefaultHandler {


   private static final String SVGROOT = "svg";
   private static final String SYMBOL = "symbol";
   private static final String VIEWBOX = "viewBox";
   private static final String IMAGE_ID = "id";

   private SVGLibrary libraryToParse;
   private StringBuilder elementValue;
   private List<SVGImageElement> svgImages;
   private boolean parsingASymbol;
   private SVGImageElement currentImage;
   private StringBuilder elementXML;

   @Override
   public void characters(char[] ch, int start, int length) throws SAXException {
       if (elementValue == null) {
           elementValue = new StringBuilder();
       } else {
           elementValue.append(ch, start, length);
       }
   }

   @Override
   public void startDocument() throws SAXException {
       //website = new Baeldung();
       svgImages = new java.util.ArrayList<SVGImageElement>();
       parsingASymbol = false;
   }

   @Override
   public void startElement(String uri, String lName, String qName, Attributes attr) throws SAXException {
       switch (qName) {

         // If we are inside a 'SYMBOL' then every single element, attribute should be appended to the 'internalXML' variable
           case SVGROOT:
               //website.articleList = new ArrayList<>();
               System.out.println("Read the SVGRoot");
               break;
           case SYMBOL:
               currentImage = new SVGImageElement();
               getSvgImages().add(currentImage);
               parsingASymbol = true;

               currentImage.setWidth(256);
               currentImage.setHeight(256);
               currentImage.setTitle(attr.getValue("id"));
               currentImage.setViewBox(attr.getValue("viewBox"));
               //System.out.println("IMAGE: " + currentImage.getTitle());
               

               break;
           default:
               if (parsingASymbol == true)
               {
                  elementValue = new StringBuilder();
                  //System.out.println("\tqName: " + qName);
                  //System.out.println("\turi: " + uri);
                  //System.out.println("\tlName: " + lName);

                  elementValue.append("<" + qName + " ");

                  for (int i=0; i < attr.getLength(); i++)
                  {
                    //System.out.println( "\t\tAttribute of " + attr.getType(i)  + ": \n" + attr.getQName(i) +
                      //attr.getLocalName(i) +            
                    //   "=\"" +         
                     // attr.getURI(i) +
                    //  attr.getValue(i) + "\"");
                    
                    elementValue.append( attr.getQName(i) + "=\"" + attr.getValue(i) + "\" ");
                  }
                  elementValue.append("/>\n");
                  currentImage.setInternalXML( currentImage.getInternalXML().concat( elementValue.toString() ) );

               }
 /**          case VIEWBOX:
               elementValue = new StringBuilder();
               break;
           case IMAGE_ID:
               elementValue = new StringBuilder();
               break;
                */
       }
   }

   @Override
   public void endElement(String uri, String localName, String qName) throws SAXException {
       switch (qName) {
           case SVGROOT:
               //latestArticle().setTitle(elementValue.toString());
               break;
           case SYMBOL:
             // viewBox="0 0 26 26" id="xchange-greyscale"
               System.out.println(currentImage.getTitle() + " : " + currentImage.getInternalXML());
               parsingASymbol = false;
               break;
       }
   }


    public List<SVGImageElement> getSvgImages() {
        return svgImages;
    }
}