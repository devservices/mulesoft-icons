package uk.co.deloitte.cloudengineering;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import uk.co.deloitte.cloudengineering.model.DrawIOImageElement;

public class SVGImageParser extends DefaultHandler {

	private static final String SVGROOT = "svg";
	private static final String VIEWBOX = "viewBox";
	private static final String WIDTH = "width";
	private static final String HEIGHT = "height";
	private static final String IMAGE_ID = "id";

	private DrawIOImageElement localElement;

	private StringBuilder elementValue;

	public void setDrawIOImage(DrawIOImageElement element) {
		localElement = element;
	}

	
	
	@Override
	public void startDocument() throws SAXException {

	}

//	<svg width="256" height="256"

	@Override
	public void startElement(String uri, String lName, String qName, Attributes attr) throws SAXException {
		switch (qName) {

		// If we are inside a 'SYMBOL' then every single element, attribute should be
		// appended to the 'internalXML' variable
		case SVGROOT:
			// website.articleList = new ArrayList<>();
			System.out.println("Read the SVGRoot");
			for (int i = 0; i < attr.getLength(); i++) {

				
				System.out.println("Name: " + attr.getQName(i) + " value: " + attr.getValue(attr.getQName(i))  );
				
				switch (attr.getQName(i)) {
				case WIDTH:
                    String width = attr.getValue(attr.getQName(i));
					localElement.setW(width);
				case HEIGHT:
					localElement.setH(attr.getValue(attr.getQName(i)));
				}

			}

			break;

		default:

			/**
			 * case VIEWBOX: elementValue = new StringBuilder(); break; case IMAGE_ID:
			 * elementValue = new StringBuilder(); break;
			 */
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		switch (qName) {
		case SVGROOT:
			// latestArticle().setTitle(elementValue.toString());
			break;

		}
	}
}
