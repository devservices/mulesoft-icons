package uk.co.deloitte.cloudengineering;


import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.xml.sax.SAXException;

import uk.co.deloitte.cloudengineering.model.SVGImageElement;

public class SAXParserMain {

public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
    SAXParserFactory factory = SAXParserFactory.newInstance();
    SAXParser saxParser = factory.newSAXParser();

    SVGLibraryParser parser = new SVGLibraryParser();
    saxParser.parse("src/main/resources/images/svg/RenderIssues/APIManager2.svg", parser);
    

    

    parser.getSvgImages().forEach( a -> writeToTempFile(a) );

 
    
//<svg xmlns='http://www.w3.org/2000/svg' width="256" height="256" viewBox="0 0 26 26" id="design-center-color">
//  <path fill="#087299" d="M7.242 17h4.76l-1.401 2h-4.4L2 13l4.201-6h4.4l1.401 2h-4.76l-2.8 4 2.8 4z"></path>
//  <path fill="#114459" d="M14.802 13l-1.4 2h-2.099l-1.401-2 1.401-2h2.099l1.4 2z"></path>
//  <path fill="#00A3E0" d="M24 13c0 5.523-4.477 10-10 10h-2.998l-1.4-2H14c4.411 0 8-3.589 8-8s-3.589-8-8-8H9.602l1.4-2H14c5.523 0 10 4.477 10 10z"></path>
//</svg>
}

private static Object writeToTempFile(SVGImageElement image) {

    StringBuilder fileContents = new StringBuilder();
    fileContents.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
    fileContents.append("<svg xmlns='http://www.w3.org/2000/svg' width=\"256\" height=\"256\" viewBox=\"" +
     image.getViewBox() + "\" " + "id=\"" + image.getTitle() + "\">\n" );
    fileContents.append( image.getInternalXML() + "\n");
    fileContents.append("</svg>");


 
    try {
        //Path tempFile = Files.createTempFile(image.getTitle(), ".svg");

        String tempPath = System.getProperty("java.io.tmpdir");
        Path tempFilePath = Paths.get(tempPath + image.getTitle() + ".svg");
        System.out.println(tempFilePath.toString());
        Path tempFile = Files.createFile(tempFilePath);

  
        Files.write(tempFile, fileContents.toString().getBytes(StandardCharsets.UTF_8));
    } catch (IOException e) {
        throw new RuntimeException(e);
    }
   
    return null;

}



}