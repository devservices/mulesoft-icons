# Deloitte Brandspace Icons

## Intro

Deloitte has a really nice set of Icons, but usually has only been provided in PPT format.

Getting these out to individual SVG's could allow the use in Plantuml, Drawio etc;


## Contact

This option was given by Mark McGrotty on the Slack channel `#templates`

## Resource Location

This Figma is the beginning of a DD UI kit, which has most of them there:

[Figma.com DD Library](https://www.figma.com/file/xWeVu5VZuuxVKFKb1xquKZ/dds-ui-kit-v2.0_dd-branch?type=design&node-id=5-236&t=DEOsxnMFNheoUbbm-0)

## Pseudo-code

### Problem

Exporting each Icon as a Group individually would take hours, but it produces the right size.

Exporting as a block of related images (make sure you tick to retain Id's) is faster... but... The export takes a set of `<g>` icons over a large bounding box such as 2000, 1800px.

Also, .. the names unfortunately aren't smart and are items like 'Group', 'Group1', 'Group2'..

If you just do a dumb write / extract of the individual `<g>`svg paths it will be a tiny icon in a very big bounding box with large whitespace.

To balance the 2 it would be export up to 20 different Top level groups like `Buildings, Engineering and Architecture` and have a scripted way of spitting out the individual icons based on the group-name to a sub-folder and new svg file that starts co-ordinates at 0,0.


### How to attack it

```
SVG: `<svg>`  
Maximum bound width: = 0,1908
Maximum bound height = 0,1074 

Top level Group `<g`  : id = BUILDINGS, ENGINEERING AND ARCHITECTURE-01

*Leaf Level-Group* `<g>`: id = 'Group'  G1 + G2 + G..
--> Create new file :  ./$G1/$G2.svg

  --> Parse Paths, Circles,.. Bounds Min X, Max X, Min Y, Max Y
  --> e.g.  located at 1750,500 to outer bound 1800,550
  --> Shift LH location to 0,0 (Point X - 1750, Y - 500)
```

Example:
```
<svg xmlns="http://www.w3.org/2000/svg" width="1908" height="1074" viewBox="0 0 1908 1074" fill="none">
  <g id="BUILDINGS, ENGINEERING AND ARCHITECTURE-01">
    <g id="Group">
      <path id="Vector" d="M1739.5 219.1C1723.8 219.1 1711.2 231.8 1711.2 247.4C1711.2 263 1723.9 275.8 1739.5 275.8C1755.2 275.8 1767.8 263.1 1767.8 247.5C1767.8 231.7 1755.2 219.1 1739.5 219.1ZM1739.5 273.7C1724.9 273.7 1713.2 261.9 1713.2 247.4C1713.2 232.8 1725 221 1739.5 221C1754.1 221 1765.8 232.8 1765.8 247.3C1765.8 261.9 1754 273.7 1739.5 273.7Z" fill="#62B5E5"></path>
      
    </g>
```

### Shift pseudo-code

One popular library for working with SVGs in JavaScript is [svg.js](https://svgjs.dev/docs/3.0/). It provides a powerful and intuitive API for manipulating SVG elements.

Here's an example of how you can use the svg.js library to move an SVG image to the top-left corner while retaining the same shape:

`npm install svg.js xmldom`

*Code sample:*
```
const SVG = require('svg.js');
const { DOMParser, XMLSerializer } = require('xmldom');

function createGroupsWithNewCoordinates(xmlString) {
  const parser = new DOMParser();
  const serializer = new XMLSerializer();

  // Parse the XML string to create an XML document object
  const doc = parser.parseFromString(xmlString, 'text/xml');

  // Get the root SVG element
  const svgElement = doc.documentElement;

  // Create a new SVG.js instance
  const svg = SVG().size(256, 256);

  // Recursive function to process nested groups
  function processGroup(groupElement, parentGroup) {
    // Create a new group element in SVG.js
    const group = parentGroup.group().id(groupElement.getAttribute('id'));

    // Loop through the child nodes of the group element
    const childNodes = groupElement.childNodes;
    for (let i = 0; i < childNodes.length; i++) {
      const node = childNodes[i];

      if (node.nodeName === 'g') {
        // Recursive call to process nested groups
        processGroup(node, group);
      } else if (node.nodeName === 'path') {
        // Get the 'd' and 'fill' attributes of the path
        const d = node.getAttribute('d');
        const fill = node.getAttribute('fill');

        // Create the path in the SVG.js group element
        group.path(d).fill(fill);
      }
    }
  }

  // Find the top-level <g> element
  const topLevelGroup = svgElement.getElementsByTagName('g')[0];

  // Process the top-level <g> element and its nested groups
  processGroup(topLevelGroup, svg);

  // Get the SVG markup of the modified SVG document
  const modifiedXmlString = serializer.serializeToString(svg.node);

  return modifiedXmlString;
}

// Example usage
const originalXmlString = `
<svg xmlns="http://www.w3.org/2000/svg" width="1908" height="1704">
  <g id="icons">
    <rect width="19633" height="4027" transform="translate(-2160 -716)" fill="white"/>
    <g id="BUSINESS &#38; OFFICE 1-01">
      <g id="Group"></g>
      <g id="Group_2">..</g>
    </g>
  </g>
</svg>`;

const modifiedXmlString = createGroupsWithNewCoordinates(originalXmlString);
console.log(modifiedXmlString);

```